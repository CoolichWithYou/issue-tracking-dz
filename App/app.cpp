#include "app.h"
#include "Color.h"
#include <conio.h>

using namespace CoolichWithYou;
using namespace std;

string txtFilesFolder = "txtFiles/";
string fileExtension = ".txt";

string login = "-";

std::vector<Subject> subjectList;

const string PATH_BIN_FILE = "subject.dir";

int nothing(){ //костыль
    return 0;
}

int printSubjectList(){
    try {
        if (subjectList.empty()) {
            throw "Товары отсутствуют!\n\n";
        }
        else{
            for (int i = 0; i < subjectList.size(); ++i) {
                setColor(Yellow, Black);
                cout << i;
                setColor(White, Black);
                cout << " " << subjectList[i].getName() << endl;
            }
            cout << "\n" << endl;
        }
    }
    catch(char * a){
        cerr << "Error: " << a;
    }
    system("pause");
    SubjectList();
}

int addNewSubject(){
    string name;
    cout << "Введите название товара:\n";

    cin >> name;

    Subject a(name);

    subjectList.push_back(a);

    fstream io_file(PATH_BIN_FILE, ios::binary | ios::out);

    for (auto &it : subjectList) {
        it.parseInBinFile(io_file);
    }

    io_file.close();

    cout << "Товар успешно добавлен!\n\n";

    system("pause");
    SubjectList();
    return 1;
}

int sortSubjectByName(){
    sortAnything(subjectList);

    if (!checkEmpty(subjectList)) {
        for (int i = 0; i < subjectList.size(); ++i) {
            setColor(Yellow,Black);
            cout << i;
            setColor(White,Black);
            cout << " " << subjectList[i].getName() << endl;
        }
        cout << "\n" << endl;
    }
    fstream io_file(PATH_BIN_FILE, ios::binary | ios::out);

    for (auto &it : subjectList) {
        it.parseInBinFile(io_file);
    }

    io_file.close();

    cout << "\n" << endl;
    system("pause");
    SubjectList();
}

int buySubject(){
    //вывести предметы
    try {
        if (subjectList.empty()) {
            throw "Товары отсутствуют!\n\n";
        }
        else{
            for (int i = 0; i < subjectList.size(); ++i) {
                setColor(Yellow, Black);
                cout << i;
                setColor(White, Black);
                cout << " " << subjectList[i].getName() << endl;
            }
            cout << "\n" << endl;
        }
    }
    catch(char * a){
        cerr << "Error: " << a;
    }
    system("pause");

    int chooseSubject{};

    cout << "Введите id товара, который хотите купить\n\n>>";
    cin >> chooseSubject;

    //узнать id пользователя

    int idOfHuman{};

    for(int i = 0; i < nicknamesList.size(); i++){
        if(login == nicknamesList[i]){
            idOfHuman = i;
        }
    }

    //записать в файл с его id покупку

    string fileAddress;

    fileAddress = txtFilesFolder + to_string(idOfHuman+1) + fileExtension;

    fstream openAndInput(fileAddress, ios_base::app);

    openAndInput.seekg(0, ios_base::end);

    //записать в файл выбранный предмет на покупку
    openAndInput << subjectList[chooseSubject].getName();
    openAndInput << "\n";

    openAndInput.close();

    //возвращаемся в меню
    SubjectList();
}

int editSubject(){
    if(!checkEmpty(subjectList)){
        auto it = subjectList.begin();

        for (int i = 0; i < subjectList.size(); ++i) {
            setColor(Yellow,Black);
            cout << i;
            setColor(White,Black);
            cout << " " << subjectList[i].getName() << endl;
        }
        cout << "\n\n";

        try {

            int changeSubjectId{};
            string newName, newSurname;
            cout << "Введите id товара, который хотите изменить:\n->>";
            cin >> changeSubjectId;

            if((changeSubjectId >= subjectList.size()) || (changeSubjectId < 0))
                throw "Вы ввели число, большее чем кол-во элементов или меньшее ноля!\n\n";
            else {

                cout << "Введите новое название товара:\n->>";
                cin >> newName;

                subjectList[changeSubjectId].setName(newName);

                fstream io_file(PATH_BIN_FILE, ios::binary | ios::out);

                for (auto &it : subjectList) {
                    it.parseInBinFile(io_file);
                }

                io_file.close();
            }
        }
        catch(const char* a){
            cerr << "Error: " << a;
        }

        cout << "Информация о товаре успешно изменена!\n\n";
    }

    system("pause");
    SubjectList();
    return 1;
}
int deleteSubject(){
    if(!checkEmpty(subjectList)){
        auto it = subjectList.begin();

        for (int i = 0; i < subjectList.size(); ++i) {
            cout << i << " " << subjectList[i].getName() << endl;
        }
        cout << "\n\n";

        int deleteSubjectId{};
        cout << "Введите id товара, который хотите удалить:\n->>";

        try {

            cin >> deleteSubjectId;

            if((deleteSubjectId >= subjectList.size()) || (deleteSubjectId < 0))
                throw "Вы ввели число, большее чем кол-во элементов или меньшее ноля!\n\n";
            else {

                subjectList.erase(subjectList.begin() + deleteSubjectId);

                fstream io_file(PATH_BIN_FILE, ios::binary | ios::out);

                for (auto &it : subjectList) {
                    it.parseInBinFile(io_file);
                }

                io_file.close();
            }
        }
        catch(char * a){
            cerr << "Error: " << a;
        }

        cout << "товар успешно удалён!\n\n";
    }

    system("pause");
    SubjectList();
    return 1;
}

bool restoreInFile() {
    try {
        fstream io_file(PATH_BIN_FILE, ios::binary | ios::in);
        if(!io_file.is_open()){
            throw "Файл не удалось открыть!\n\n";
        }

        Subject subject{};
        while (subject.unparseFromBinFile(io_file)) {
            subjectList.push_back(subject);
        }

        io_file.close();
    }
    catch(const char* a){
        cerr << "Error: " << a;
    }
    return true;
}

//easyExam


int countOfTopic() {
    int count = 0;
    char fileStream;

    fstream allPuncts("nicknames.txt");
    while (allPuncts.get(fileStream)) {
        if (fileStream == '\n') {
            count++;
        }
    }
    allPuncts.close();

    return count + 1;
}

string chooseFile() {
    int j = 0;
    int Puncts = countOfTopic();

    string drawnPoint = "-> ";
    fstream allPuncts("nicknames.txt");
    string jLocation;

    string* punctsList = new string[Puncts];

    for (int i = 0; i < Puncts; i++) {
        getline(allPuncts, punctsList[i]);
    }


    for (int i = 0; i < Puncts; i++) {
        if (i == j) {
            setColor(Yellow, Black);
            cout << setw(5) << drawnPoint + punctsList[i] << endl;
            setColor(White, Black);
        }
        else {
            cout << setw(5) << punctsList[i] << endl;
        }
    }

    char selectingFile = 'a';

    while (selectingFile != 13) {
        selectingFile = _getch();
        system("cls");
        if (selectingFile == 's') {
            j++;
            if (j >= Puncts) {
                j = 0;
            }
        }
        if (selectingFile == 'w') {
            j--;
            if (j < 0) {
                j = Puncts;
            }
        }
        for (int i = 0; i < Puncts; i++) {
            if (i == j) {
                setColor(Yellow, Black);
                cout << drawnPoint + punctsList[i] << endl;
                setColor(White, Black);
            }
            else {
                cout << setw(5) << punctsList[i] << endl;
            }
        }
        jLocation = to_string(j+1);
    }
    allPuncts.close();
    delete [] punctsList;
    return jLocation;
}
int openChoosedFile() {
    string choose = chooseFile();

    string chooseFix = choose;

    choose = txtFilesFolder + choose + fileExtension; // (j+1)+.txt
    fstream task(choose);

    system("cls"); //clear window

    char fileStream;

    outputThemeName(chooseFix);

    while (task.get(fileStream)) {
        cout << fileStream;
    }

    task.close();

    char enterWaiting = 'a';

    while (enterWaiting != 13) {
        enterWaiting = _getch();
    }

    system("cls");
    SubjectList();
    //nothing();
}

void txtFilesCreator() {
    string fileAddress;
    int fileCount = countOfTopic();
    for (int i = 0; i < fileCount; i++) {
        fileAddress = txtFilesFolder + to_string(i+1) + fileExtension;
        ofstream checkAvailability(fileAddress, ios_base::app);
        checkAvailability.close();
    }
}

void outputThemeName(string chooseTheme) {
    int themeNumber = atoi(chooseTheme.c_str());

    int Puncts = countOfTopic();

    fstream allPuncts("nicknames.txt");

    string* punctsList = new string[Puncts];

    for (int i = 0; i <= Puncts; i++) {
        getline(allPuncts, punctsList[i]);
        if (i == themeNumber) {
            setColor(Yellow, Black);
            cout << setw(5) << punctsList[i - 1] << endl;
            setColor(White, Black);
        }
    }

    delete[] punctsList;
    allPuncts.close();
}

int checkSubjectBuyList(){
    //узнаём id пользователя
    int idOfHuman{};

    for(int i = 0; i < nicknamesList.size(); i++){
        if(login == nicknamesList[i]){
            idOfHuman = i;
        }
    }

    string choose = to_string(idOfHuman+1);

    string chooseFix = choose;

    choose = txtFilesFolder + choose + fileExtension; // (j+1)+.txt
    fstream task(choose);

    system("cls"); //clear window

    char fileStream;

    outputThemeName(chooseFix);

    while (task.get(fileStream)) {
        cout << fileStream;
    }

    task.close();

    char enterWaiting = 'a';

    while (enterWaiting != 13) {
        enterWaiting = _getch();
    }

    system("cls");
    SubjectList();

}