//
// Created by Test-notebook on 5/28/2021.
//

#include "InterfaceScreen.h"

using namespace CoolichWithYou;

int SubjectList() {
    system("cls");
    if(login == "admin") {

        CMenuItem items[7]{
                CMenuItem{"Отобразить список товаров", printSubjectList},
                CMenuItem{"Сортировать по названию", sortSubjectByName},
                CMenuItem{"Добавить новый товар в список", addNewSubject},
                CMenuItem{"Редактировать товар по id", editSubject},
                CMenuItem{"Удалить товар по id", deleteSubject},
                CMenuItem{"Просмотреть купленные пользователями товары", openChoosedFile},
                CMenuItem{"Вернуться назад", runMenu}
        };

        CMenu menu("Subject menu", items, 7);

        menu.runCommand();
    }
    else{
        CMenuItem items[5]{
                CMenuItem{"Отобразить список товаров", printSubjectList},
                CMenuItem{"Сортировать по названию", sortSubjectByName},
                CMenuItem{"Купить товар", buySubject},
                CMenuItem{"Просмотреть купленные товары", checkSubjectBuyList},
                CMenuItem{"Вернуться назад", runMenu}
        };

        CMenu menu("Subject menu", items, 5);

        menu.runCommand();
    }
    return 1;
}

int authorizathion(){
    login = "-";
    while(login == "-") {
        auth();
    }

    runMenu();
}

int runMenu(){
    if(login == "admin"){
        adminRun();
    }
    else{
        usualRun();
    }
}

void adminRun(){
    system("cls");
    CMenuItem items[2]{CMenuItem{"Взаимодействия с товарами", SubjectList},
                       CMenuItem{"Разлогиниться", authorizathion}};
    CMenu menu("My console menu", items, 2);

    menu.runCommand();
}

void usualRun(){
    system("cls");
    CMenuItem items[2]{CMenuItem{"Перейти к списку возможностей", SubjectList},
                       CMenuItem{"Разлогиниться", authorizathion}};
    CMenu menu("My console menu", items, 2);

    menu.runCommand();
}