# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/Test-notebook/Desktop/opi-dz/Containers/list.cpp" "C:/Users/Test-notebook/Desktop/opi-dz/cmake-build-debug/Containers/CMakeFiles/Containers.dir/list.cpp.obj"
  "C:/Users/Test-notebook/Desktop/opi-dz/Containers/mstring.cpp" "C:/Users/Test-notebook/Desktop/opi-dz/cmake-build-debug/Containers/CMakeFiles/Containers.dir/mstring.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../Models"
  "../Containers"
  "../MyConMenu"
  "../Store"
  "../Screens"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
